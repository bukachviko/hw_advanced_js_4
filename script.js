//AJAX- Asynchronous JavaScript And XML— це підхід до побудови користувацьких
// інтерфейсів вебзастосунків, за яких вебсторінка, не перезавантажуючись,
// у фоновому режимі надсилає запити на сервер і сама звідти довантажує
// потрібні користувачу дані.
//чим він корисний при розробці : дoзволяє отримувати данні з сервера
// та відправляти дані форми на сервер

'use strict'

const filmsDiv = document.querySelector('.js-films');
var countResponsesTotal = 0;
var countResponses = 0;
const preloader = document.querySelector('.preloader-js');
preloader.style.display = 'block';
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        // console.log(films)
        films.forEach(film => {
            let charterName = " ";

            filmsDiv.insertAdjacentHTML('beforeend', `<li>
                          <h2>Назва фльму: "${film.name}"</h2>
                          <h4>Номер епізоду: "${film.episodeId}"</h4>
                          <h4>Короткий зміст: <p>"${film.openingCrawl}"</p></h4>
                          <ul id="${film.id}">Персонажі: </ul>
                      </li>`);
            let ul = document.getElementById(`${film.id}`);

            countResponsesTotal+=film.characters.length

            film.characters.forEach(charactersURL => {
                fetch(charactersURL)
                    .then(res => res.json())
                    .then(data => {
                        // console.log(data.name)
                        countResponses++
                        // console.log(countResponses)
                        if(countResponses === countResponsesTotal) {
                            preloader.style.display = 'none';
                            console.log(film.characters + ' Завантажено все')
                        }
                        ul.insertAdjacentHTML('beforeend', `<li>${data.name}</li>`)
                    })
            })
        })
    }).catch(err => {
    alert(err.message)
})










